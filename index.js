// Create Single Room
db.hotel.insertOne(
    {
        name: "Single",
        accomodate: 2,
        price: 1000,
        description: "A simple room with all the basic necessities",
        rooms_available: 10,
        isAvailable: false
    }
)

// Insert Multiple Rooms
db.hotel.insertMany([
    {
        name: "Double",
        accomodate: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation",
        rooms_available: 5,
        isAvailable: false
    },
    {
        name: "Queen",
        accomodate: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: 15,
        isAvailable: false
    }
]);

// search for a room with the name "Double"
db.hotel.find({name: "Double"});


// updateOne the queen room and set the available rooms to 0
db.hotel.updateOne(
    {
        name: "Queen"
    },
    {
        $set: {
            rooms_available: 0,
        }
    }
)

// deleteMany all the rooms that have 0 availability
db.hotel.deleteMany({rooms_available: 0});
